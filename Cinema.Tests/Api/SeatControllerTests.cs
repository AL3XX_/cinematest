using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Cinema.Models.Seats;
using Cinema.Tests.Extensions;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc.Testing;
using Xunit;

namespace Cinema.Tests.Api
{
    public class SeatControllerTests : BaseTestServer
    {
        public SeatControllerTests(WebApplicationFactory<Startup> factory) : base(factory)
        {
        }

        [Fact(DisplayName = "Method should not return any exception if count parameter is 0 or null.")]
        public async Task GetList_ShouldNotReturnErrorIfCountIsNull()
        {
            var response = await ApiClient.GetAsync("/api/v1/seat");
            response.IsSuccessStatusCode.Should().BeTrue();
        }

        [Fact(DisplayName = "Method should successfully return seats.")]
        public async Task GetList_ShouldReturnSeats()
        {
            var request = await ApiClient.GetAsync("seat", "?count=10");
            var response = await request.ParseResponseAsync<List<SeatModel>>();
            response.Count.Should().NotBe(0);
        }

        [Fact(DisplayName = "Method should return error if seat model have null properties.")]
        public async Task Reserve_ShouldReturnErrorIfSeatModelHaveNullProperties()
        {
            var reservationModel = new ReserveSeatModel();
            var response = await ApiClient.PostAsync("seat", string.Empty, reservationModel);

            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);
        }
        
        [Fact(DisplayName = "Method should return error if seatId has wrong type.")]
        public async Task Reserve_ShouldReturnErrorIfSeatIdHasWrongType()
        {
            var reservationModel = new { seatId = "1234" };
            var response = await ApiClient.PostAsync("seat", string.Empty, reservationModel);

            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);
        }   
        
        [Fact(DisplayName = "Method should return error if seatId has negative number.")]
        public async Task Reserve_ShouldReturnErrorIfSeatIdHasNegativeNumber()
        {
            var reservationModel = new ReserveSeatModel
            {
                SeatId = -1
            };
            var response = await ApiClient.PostAsync("seat", string.Empty, reservationModel);

            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);
        }      
        
        [Fact(DisplayName = "Method should return error if seatId has non existing value.")]
        public async Task Reserve_ShouldReturnErrorIfSeatIdHasNonExistingValue()
        {
            var reservationModel = new ReserveSeatModel
            {
                SeatId = new Random().Next(12345, 143122)
            };
            var response = await ApiClient.PostAsync("seat", string.Empty, reservationModel);

            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);
        }
    }
}