using System;
using System.Net.Http;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.AspNetCore.TestHost;
using Xunit;

namespace Cinema.Tests
{
    public class BaseTestServer : IClassFixture<WebApplicationFactory<Startup>>
    {
        private readonly WebApplicationFactory<Startup> factory;
        protected HttpClient ApiClient;

        protected TestServer Server;

        public BaseTestServer(WebApplicationFactory<Startup> factory)
        {
            this.factory = factory;
            SetUpClient();
        }

        private void SetUpClient()
        {
            ApiClient = factory.CreateClient();
            Server = factory.Server;
            ApiClient.BaseAddress = new Uri($"{ApiClient.BaseAddress}api/v1/");
        }
    }
}