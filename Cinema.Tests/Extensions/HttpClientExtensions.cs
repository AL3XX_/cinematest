using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Cinema.Infrastructure.Models;
using Newtonsoft.Json;

namespace Cinema.Tests.Extensions
{
    public static class HttpClientExtensions
    {
        public static async Task<HttpResponseMessage> GetAsync(this Task<HttpClient> client, string controller,
            string endpoint)
        {
            return await (await client).GetAsync(controller, endpoint);
        }

        public static async Task<HttpResponseMessage> PostAsync(this Task<HttpClient> client, string controller,
            string endpoint, object data)
        {
            return await (await client).PostAsync(controller, endpoint, data);
        }

        public static async Task<HttpResponseMessage> GetAsync(this HttpClient client, string controller,
            string endpoint)
        {
            var requestUri = $"{controller}/{endpoint}";
            return await client.GetAsync(requestUri);
        }

        public static async Task<HttpResponseMessage> PostAsync(this HttpClient client, string controller,
            string endpoint, object data)
        {
            var requestUri = $"{controller}/{endpoint}";
            return await client.PostAsync(requestUri, data.ToJsonContent());
        }

        public static async Task<CommandResult> ParseResponseAsync(this HttpResponseMessage message)
        {
            var responseString = await message.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<CommandResult>(responseString);
        }    
        
        public static async Task<TModel> ParseResponseAsync<TModel>(this HttpResponseMessage message)
        {
            var responseString = await message.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<TModel>(responseString);
        }

        private static HttpContent ToJsonContent(this object content)
        {
            var requestString = JsonConvert.SerializeObject(content);
            return new StringContent(requestString, Encoding.UTF8, "application/json");
        }
    }
}