using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Autofac;
using Cinema.Common.Configurations;
using Cinema.Data.Contexts;
using Cinema.Data.Entities;
using Cinema.Data.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Cinema.Data.Initializers
{
    public class DbInitializerSeats
    {
        public static async Task InitializeSeats(ILifetimeScope scope)
        {
            var repository = scope.Resolve<IRuntimeRepository>();
            var existingSeats = await repository.Query<SeatEntity>().ToListAsync();
            var seatsInitializationNumber = Settings.Instance.DefaultSeatsNumber;

            if (existingSeats.Count > seatsInitializationNumber)
            {
                return;
            }

            seatsInitializationNumber -= existingSeats.Count;
            var seatsCollection = new List<SeatEntity>();

            for (var i = 0; i < seatsInitializationNumber; i++)
            {
                seatsCollection.Add(new SeatEntity());
            }
            
            await repository.AddAsync(seatsCollection);
            await repository.SaveAsync<SeatEntity>();
        }
    }
}