using System.Linq;
using Autofac;
using Cinema.Data.Contexts;
using Microsoft.EntityFrameworkCore;

namespace Cinema.Data.Initializers
{
    public static class DbInitializer
    {
        public static void InitializeDatabase(ILifetimeScope scope)
        {
            var dbContexts = typeof(ApplicationDbContext)
                .Assembly
                .GetTypes()
                .Where(t => t.IsSubclassOf(typeof(DbContext)) && !t.IsAbstract)
                .Select(t => scope.Resolve(t) as DbContext)
                .ToList();

            foreach (var dbContext in dbContexts)
            {
                dbContext?.Database.Migrate();
            }
        }
    }
}