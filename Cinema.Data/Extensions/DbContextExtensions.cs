using System;
using System.Linq;
using Cinema.Data.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Cinema.Data.Extensions
{
    public static class DbContextExtensions
    {
        public static void IncludeEntityFieldsAudition(this DbContext context)
        {
            var entities = context.ChangeTracker
                .Entries()
                .Where(x => x.Entity is IEntityBase &&
                            (x.State == EntityState.Added || x.State == EntityState.Modified));

            foreach (var entity in entities)
            {
                var entityBase = (IEntityBase) entity.Entity;

                if (entity.State == EntityState.Added) entityBase.CreatedOn = DateTime.UtcNow;

                entityBase.UpdatedOn = DateTime.UtcNow;
            }
        }

        public static void RemoveById<TEntity>(this DbContext context, long id)
            where TEntity : class, IEntityBase, new()
        {
            var entryToDelete = new TEntity { Id = id };
            context.Entry(entryToDelete).State = EntityState.Deleted;
        }
    }
}