using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Cinema.Data.Contexts;
using Cinema.Data.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Cinema.Data.Repositories
{
    public class RuntimeRepository : IRuntimeRepository
    {
        private readonly ApplicationDbContext context;

        public RuntimeRepository(ApplicationDbContext context)
        {
            this.context = context;
        }

        public IQueryable<TEntity> Query<TEntity>() where TEntity : class, IEntityBase => GetQueryableDbSet<TEntity>();

        public Task<TEntity> GetAsync<TEntity>(long id, CancellationToken cancellationToken = default) where TEntity : class, IEntityBase => GetQueryableDbSet<TEntity>().FirstOrDefaultAsync(e => e.Id == id, cancellationToken);

        public async Task AddAsync<TEntity>(TEntity entity, CancellationToken cancellationToken = default) where TEntity : class, IEntityBase => await GetDbSet<TEntity>().AddAsync(entity, cancellationToken);

        public async Task AddAsync<TEntity>(IEnumerable<TEntity> entities, CancellationToken cancellationToken = default) where TEntity : class, IEntityBase => await GetDbSet<TEntity>().AddRangeAsync(entities, cancellationToken);

        public Task DeleteAsync<TEntity>(TEntity entity, CancellationToken cancellationToken = default) where TEntity : class, IEntityBase
        {
            GetDbSet<TEntity>().Remove(entity);
            return Task.CompletedTask;
        }

        public async Task<int> SaveAsync<TEntity>(CancellationToken cancellationToken = default) where TEntity : class, IEntityBase
        {
            return await context.SaveChangesAsync(cancellationToken);
        }

        private IQueryable<TEntity> GetQueryableDbSet<TEntity>() where TEntity : class, IEntityBase => GetDbSet<TEntity>().AsQueryable();

        private DbSet<TEntity> GetDbSet<TEntity>() where TEntity : class, IEntityBase => context.Set<TEntity>();
    }
}