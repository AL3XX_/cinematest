﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Autofac;
using Cinema.Data.Contexts;
using Cinema.Data.Extensions;
using Cinema.Data.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Cinema.Data.Repositories
{
    public class ApplicationRepository<TEntity, TContext> : IRepository<TEntity>
        where TContext : DbContext 
        where TEntity : class, IEntityBase, new()
    {
        private readonly TContext context;
        private readonly DbSet<TEntity> dbSet;

        public ApplicationRepository(TContext context)
        {
            this.context = context;
            dbSet = context.Set<TEntity>();
        }

        public IQueryable<TEntity> Query => dbSet.AsQueryable();
        
        public async Task<TEntity> GetAsync(long id, CancellationToken cancellationToken = default)
        {
            return await dbSet.FindAsync(id);
        }

        public async Task AddAsync(TEntity entity, CancellationToken cancellationToken = default)
        {
            await dbSet.AddAsync(entity, cancellationToken);
        }

        public async Task AddAsync(IEnumerable<TEntity> entities, CancellationToken cancellationToken = default)
        {
            await dbSet.AddRangeAsync(entities, cancellationToken);
        }

        public Task DeleteAsync(long id, CancellationToken cancellationToken = default)
        {
            context.RemoveById<TEntity>(id);
            return Task.CompletedTask;
        }

        public async Task<int> SaveAsync(CancellationToken cancellationToken = default)
        {
            return await context.SaveChangesAsync(cancellationToken);
        }
    }
}