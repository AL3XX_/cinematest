using System;
using System.ComponentModel.DataAnnotations.Schema;
using Cinema.Data.Interfaces;

namespace Cinema.Data.Models
{
    public class EntityBase : IEntityBase
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        public DateTime? CreatedOn { get; set; }

        public DateTime? UpdatedOn { get; set; }

        public bool IsDeleted { get; set; }
    }
}