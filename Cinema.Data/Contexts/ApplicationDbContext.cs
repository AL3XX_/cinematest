using System.Threading;
using System.Threading.Tasks;
using Cinema.Data.Entities;
using Cinema.Data.Extensions;
using Microsoft.EntityFrameworkCore;

namespace Cinema.Data.Contexts
{
    public class ApplicationDbContext : DbContext
    {
        public DbSet<SeatEntity> Seats { get; set; }
        
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
        }

        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = new CancellationToken())
        {
            this.IncludeEntityFieldsAudition();
            return base.SaveChangesAsync(cancellationToken);
        }
    }
}