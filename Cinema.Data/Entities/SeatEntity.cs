using System;
using System.ComponentModel.DataAnnotations.Schema;
using Cinema.Data.Models;

namespace Cinema.Data.Entities
{
    [Table("Seats")]
    public class SeatEntity : EntityBase
    {
        public DateTime? ReservedAt { get; set; }
    }
}