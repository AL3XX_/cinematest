using System;

namespace Cinema.Data.Interfaces
{
    public interface IEntityBase
    {
        long Id { get; set; }

        DateTime? CreatedOn { get; set; }

        DateTime? UpdatedOn { get; set; }

        bool IsDeleted { get; set; }
    }
}