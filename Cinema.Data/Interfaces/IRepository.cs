using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Cinema.Data.Interfaces
{
    public interface IRepository<TEntity> where TEntity : class, IEntityBase
    {
        IQueryable<TEntity> Query { get; }

        Task<TEntity> GetAsync(long id, CancellationToken cancellationToken = default);

        Task AddAsync(TEntity entity, CancellationToken cancellationToken = default);
        
        Task AddAsync(IEnumerable<TEntity> entities, CancellationToken cancellationToken = default);

        Task DeleteAsync(long id, CancellationToken cancellationToken = default);
        
        Task<int> SaveAsync(CancellationToken cancellationToken = default);
    }
}