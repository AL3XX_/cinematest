using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Cinema.Data.Interfaces
{
    public interface IRuntimeRepository
    {
        IQueryable<TEntity> Query<TEntity>() where TEntity : class, IEntityBase;

        Task<TEntity> GetAsync<TEntity>(long id, CancellationToken cancellationToken = default) where TEntity : class, IEntityBase;

        Task AddAsync<TEntity>(TEntity entity, CancellationToken cancellationToken = default) where TEntity : class, IEntityBase;
        
        Task AddAsync<TEntity>(IEnumerable<TEntity> entities, CancellationToken cancellationToken = default) where TEntity : class, IEntityBase;

        Task DeleteAsync<TEntity>(TEntity entity, CancellationToken cancellationToken = default) where TEntity : class, IEntityBase;
        
        Task<int> SaveAsync<TEntity>(CancellationToken cancellationToken = default) where TEntity : class, IEntityBase;
    }
}