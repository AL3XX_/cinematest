using System;
using System.Linq;
using System.Reflection;
using AutoMapper;
using Cinema.Common.Configurations;
using Microsoft.Extensions.DependencyInjection;

namespace Cinema.Common.Initializers
{
    public static class MapperProfilesInitializer
    {
        public static MapperConfiguration Configuration;

        public static void InitializeMapperProfiles(IServiceCollection services)
        {
            var profiles = Assembly.GetEntryAssembly()?
                .DefinedTypes
                .Where(t => t.IsSubclassOf(typeof(Profile)))
                .ToList();

            Configuration = new MapperConfiguration(expression => expression.AddMaps(Assembly.GetEntryAssembly()));
            profiles?.ForEach(p => services.AddAutoMapper(p));
        }

        public static void InitializeTestMapperProfiles(IServiceCollection services)
        {
            var assemblies = AppDomain.CurrentDomain.GetAssemblies();
            var allTypes = assemblies
                .Where(a => a.FullName.Contains(Settings.Instance.RootProjectNamespace))
                .SelectMany(a => a.DefinedTypes)
                .Where(t => t.IsSubclassOf(typeof(Profile)))
                .ToList();

            Configuration = new MapperConfiguration(expression => expression.AddMaps(assemblies));
            allTypes.ForEach(a => services.AddAutoMapper(a));
        }
    }
}