namespace Cinema.Common.Configurations
{
    public class Settings
    {
        public Settings()
        {
            Instance = this;
        }

        public static Settings Instance { get; set; }

        public string DbConnectionString { get; set; }

        public string RootProjectNamespace { get; set; }
        
        public int DefaultSeatsNumber { get; set; }
    }
}