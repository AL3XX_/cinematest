using System;
using System.Linq;
using System.Reflection;
using Autofac;
using Cinema.Common.Configurations;
using Module = Autofac.Module;

namespace Cinema.Infrastructure.Modules
{
    public class InfrastructureModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            var entry = Assembly.GetEntryAssembly();

            var assemblies = entry?
                .GetReferencedAssemblies()
                .Where(a => a.FullName.Contains(Settings.Instance.RootProjectNamespace))
                .Select(Assembly.Load)
                .ToList();

#if INTEGRATIONTESTS
            assemblies = AppDomain.CurrentDomain
                .GetAssemblies()
                .Where(a => a.FullName.Contains(Settings.Instance.RootProjectNamespace))
                .ToList();
#endif

            assemblies.Add(entry);
            builder.RegisterAssemblyTypes(assemblies.ToArray()).AsImplementedInterfaces();
        }
    }
}