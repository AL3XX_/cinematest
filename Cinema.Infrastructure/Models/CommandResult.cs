namespace Cinema.Infrastructure.Models
{
    public class CommandResult
    {
        public CommandResult(bool isSuccessful, string message = "", object addition = null)
        {
            IsSuccessful = isSuccessful;
            Message = message;
            Addition = addition;
        }

        public bool IsSuccessful { get; set; }

        public string Message { get; set; }

        public object Addition { get; set; }

        public static CommandResult Success => new CommandResult(true);

        public static CommandResult SuccessWithAddition(object addition)
        {
            return new CommandResult(true, string.Empty, addition);
        }

        public static CommandResult Error => new CommandResult(false);
    }
}