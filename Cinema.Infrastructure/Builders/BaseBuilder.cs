using Autofac;

namespace Cinema.Infrastructure.Builders
{
    public abstract class BaseBuilder
    {
        protected readonly IComponentContext ComponentContext;

        protected BaseBuilder(IComponentContext componentContext)
        {
            ComponentContext = componentContext;
        }
    }
}