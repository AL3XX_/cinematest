using System;
using System.Threading.Tasks;
using Autofac;
using Cinema.Infrastructure.Interfaces;

namespace Cinema.Infrastructure.Builders
{
    public class QueryBuilder : BaseBuilder, IQueryBuilder
    {
        public QueryBuilder(IComponentContext componentContext) : base(componentContext)
        {
        }

        public async Task<TResult> AskAsync<TContext, TResult>(TContext context) where TContext : IQueryContext
        {
            var service = ComponentContext.Resolve<IQuery<TContext, TResult>>();
            return await service.QueryAsync(context);
        }

        public async Task<TResult> AskAsync<TContext, TResult>(params object[] args) where TContext : IQueryContext
        {
            var queryService = ComponentContext.Resolve<IQuery<TContext, TResult>>();

            try
            {
                var contextInstance = (TContext) Activator.CreateInstance(typeof(TContext), args);
                return await queryService.QueryAsync(contextInstance);
            }
            catch (Exception ex)
            {
                throw new Exception(
                    $"Error occured while resolving {string.Join(",", args)} in {typeof(TContext).Name}. \n {ex}");
            }
        }
    }
}