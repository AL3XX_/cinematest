using System;
using System.Threading.Tasks;
using Autofac;
using Cinema.Infrastructure.Interfaces;
using Cinema.Infrastructure.Models;

namespace Cinema.Infrastructure.Builders
{
    public class CommandBuilder : BaseBuilder, ICommandBuilder
    {
        public CommandBuilder(IComponentContext componentContext) : base(componentContext)
        {
        }

        public async Task<CommandResult> ExecuteAsync<TContext>(TContext context) where TContext : ICommandContext
        {
            var service = ComponentContext.Resolve<ICommand<TContext>>();
            return await service.ExecuteAsync(context);
        }

        public async Task<CommandResult> ExecuteAsync<TContext>(params object[] args) where TContext : ICommandContext
        {
            var commandService = ComponentContext.Resolve<ICommand<TContext>>();

            var contextInstance = (TContext) Activator.CreateInstance(typeof(TContext), args);
            return await commandService.ExecuteAsync(contextInstance);
        }
    }
}