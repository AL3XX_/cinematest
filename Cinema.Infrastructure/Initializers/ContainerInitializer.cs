using Autofac;
using Cinema.Infrastructure.Modules;

namespace Cinema.Infrastructure.Initializers
{
    public static class ContainerInitializer
    {
        public static void Initialize(ContainerBuilder containerBuilder)
        {
            containerBuilder.RegisterModule<InfrastructureModule>();
        }
    }
}