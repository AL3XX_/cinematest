using System.Threading.Tasks;
using Cinema.Infrastructure.Models;

namespace Cinema.Infrastructure.Interfaces
{
    public interface ICommandBuilder
    {
        Task<CommandResult> ExecuteAsync<TContext>(TContext context) where TContext : ICommandContext;

        Task<CommandResult> ExecuteAsync<TContext>(params object[] args) where TContext : ICommandContext;
    }
}