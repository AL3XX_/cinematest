using System.Threading.Tasks;

namespace Cinema.Infrastructure.Interfaces
{
    public interface IQueryBuilder
    {
        Task<TResult> AskAsync<TContext, TResult>(TContext context) where TContext : IQueryContext;

        Task<TResult> AskAsync<TContext, TResult>(params object[] args) where TContext : IQueryContext;
    }
}