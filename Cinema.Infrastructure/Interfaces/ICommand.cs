using System.Threading.Tasks;
using Cinema.Infrastructure.Models;

namespace Cinema.Infrastructure.Interfaces
{
    public interface ICommand<in TContext> where TContext : ICommandContext
    {
        Task<CommandResult> ExecuteAsync(TContext context);
    }
}