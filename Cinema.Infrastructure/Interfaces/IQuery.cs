using System.Threading.Tasks;

namespace Cinema.Infrastructure.Interfaces
{
    public interface IQuery<in TContext, TResult> where TContext : IQueryContext
    {
        Task<TResult> QueryAsync(TContext context);
    }
}