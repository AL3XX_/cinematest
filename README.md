# Goal

```
A cinema theatre has forty rows, each row has twenty seats in it. Build a web page to book allocated seats on a cinema seating plan.
```

# Short architecture description.

Solution have two sides - Client and Server

1. Client side written on Angular with TypeScript.

2. Server side written on C# with ASP.NET Core 3.1 framework using Repository, CQS, Singleton patterns.

# Prerequisites

To launch application, you need to have:

1. .NET Core 3.1 runtime, sdk.
2. ASP.NET Core 3.1
3. PostgreSQL on the local machine, or any remote db. Connection strings can be configured in appsettings.json
4. Angular CLI

# Launching application

1. You need to build and launch backend from IDE or dotnet CLI. Database will initalize automatically.
 Default server address is - http://localhost:5000/. To use API directly, you can use Swagger accessed by /docs endpoint.

2. You need run ``` npm install ``` before first launch to refresh all packages.

3. You need to launch client by command - ``` ng serve --open ```.

Also, you can configure cinema properties in ``` app.configuration.ts ```.

# Testing
To launch unit-tests, you should change solution configuration to IntegrationTests.