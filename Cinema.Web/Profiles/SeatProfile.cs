using AutoMapper;
using Cinema.Data.Entities;
using Cinema.Models.Seats;

namespace Cinema.Profiles
{
    public class SeatProfile : Profile
    {
        public SeatProfile()
        {
            CreateMap<SeatEntity, SeatModel>()
                .ForMember(x => x.Id, y => y.MapFrom(m => m.Id))
                .ForMember(x => x.IsReserved, y => y.MapFrom(m => m.ReservedAt != null));
        }
    }
}