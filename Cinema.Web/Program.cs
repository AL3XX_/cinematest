﻿using Autofac.Extensions.DependencyInjection;
using Cinema.Data.Contexts;
using Cinema.Infrastructure.Initializers;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Cinema
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().MigrateData().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args)
        {
            return Host.CreateDefaultBuilder(args)
                .UseServiceProviderFactory(new AutofacServiceProviderFactory(ContainerInitializer.Initialize))
                .ConfigureWebHostDefaults(configuration => { configuration.UseStartup<Startup>(); });
        }
    }

    internal static class ProgramExtensions
    {
        public static IHost MigrateData(this IHost host)
        {
            using (var scope = host.Services.CreateScope())
            {
                scope.ServiceProvider.GetRequiredService<ApplicationDbContext>().Database.Migrate();
            }

            return host;
        }
    }
}