using System;
using System.Threading.Tasks;
using Cinema.Data.Entities;
using Cinema.Data.Interfaces;
using Cinema.Infrastructure.Interfaces;
using Cinema.Infrastructure.Models;
using Microsoft.EntityFrameworkCore;

namespace Cinema.Commands.Seats
{
    public class ReserveSeatCommand : ICommand<ReserveSeatCommandContext>
    {
        private readonly IRuntimeRepository runtimeRepository;

        public ReserveSeatCommand(IRuntimeRepository runtimeRepository)
        {
            this.runtimeRepository = runtimeRepository;
        }

        public async Task<CommandResult> ExecuteAsync(ReserveSeatCommandContext context)
        {
            var seat = await runtimeRepository
                .Query<SeatEntity>()
                .FirstOrDefaultAsync(s => s.Id == context.SeatId);

            if (seat == null)
            {
                return CommandResult.Error;
            }

            if (seat.ReservedAt != null)
            {
                seat.ReservedAt = null;
            }
            else
            {
                seat.ReservedAt = DateTime.UtcNow;
            }

            await runtimeRepository.SaveAsync<SeatEntity>();
            return CommandResult.Success;
        }
    }
}