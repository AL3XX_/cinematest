using Cinema.Infrastructure.Interfaces;

namespace Cinema.Commands.Seats
{
    public class ReserveSeatCommandContext : ICommandContext
    {
        public ReserveSeatCommandContext(long seatId)
        {
            SeatId = seatId;
        }
        
        public long SeatId { get; }
    }
}