using System;
using System.IO;
using System.Reflection;
using Autofac.Extensions.DependencyInjection;
using Cinema.Common.Configurations;
using Cinema.Common.Initializers;
using Cinema.Data.Contexts;
using Cinema.Data.Initializers;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;

namespace Cinema
{
    public class Startup
    {

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }


        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var settings = new Settings();
            Configuration.Bind(nameof(Settings), settings);

            services.AddSingleton(settings);
            
#if !INTEGRATIONTESTS
            MapperProfilesInitializer.InitializeMapperProfiles(services);
            services.AddDbContext<ApplicationDbContext>(builder => 
                builder.UseNpgsql(Settings.Instance.DbConnectionString), ServiceLifetime.Transient);
#else
            MapperProfilesInitializer.InitializeTestMapperProfiles(services);
            services.AddDbContext<ApplicationDbContext>(builder =>
            {
                builder.UseInMemoryDatabase(Settings.Instance.RootProjectNamespace);
            });
#endif
            
            services.AddControllers();
            
            services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc("v1", new OpenApiInfo
                {
                    Title = "Web API",
                    Version = "v1"
                });
                
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                options.IncludeXmlComments(xmlPath);
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseSwagger();
            app.UseSwaggerUI(options =>
            {
                options.SwaggerEndpoint("/swagger/v1/swagger.json", "API V1");
                options.RoutePrefix = "docs";
            });
            
            app.UseHttpsRedirection();
            app.UseRouting();
            app.UseCors(builder => builder.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader());

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
            
#if !INTEGRATIONTESTS
            DbInitializer.InitializeDatabase(app.ApplicationServices.GetAutofacRoot());
#endif
            
            DbInitializerSeats.InitializeSeats(app.ApplicationServices.GetAutofacRoot());
        }
    }
}