using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper.QueryableExtensions;
using Cinema.Common.Initializers;
using Cinema.Models.Seats;
using Cinema.Data.Entities;
using Cinema.Data.Interfaces;
using Cinema.Infrastructure.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Cinema.Queries.Seats
{
    public class GetSeatsQuery : IQuery<GetSeatsQueryContext, List<SeatModel>>
    {
        private readonly IRuntimeRepository runtimeRepository;

        public GetSeatsQuery(IRuntimeRepository runtimeRepository)
        {
            this.runtimeRepository = runtimeRepository;
        }

        public Task<List<SeatModel>> QueryAsync(GetSeatsQueryContext context)
        {
            return runtimeRepository
                .Query<SeatEntity>()
                .Where(s => !s.IsDeleted)
                .ProjectTo<SeatModel>(MapperProfilesInitializer.Configuration)
                .OrderBy(s => s.Id)
                .Take(context.Count)
                .ToListAsync();
        }
    }
}