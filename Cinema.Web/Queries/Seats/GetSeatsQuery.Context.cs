using Cinema.Infrastructure.Interfaces;

namespace Cinema.Queries.Seats
{
    public class GetSeatsQueryContext : IQueryContext
    {
        public GetSeatsQueryContext(int count)
        {
            Count = count;
        }

        public int Count { get; }
    }
}