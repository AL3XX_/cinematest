using Cinema.Infrastructure.Interfaces;
using Cinema.Infrastructure.Models;
using Microsoft.AspNetCore.Mvc;

namespace Cinema.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public abstract class BaseApiController : Controller
    {
        protected ICommandBuilder CommandBuilder;

        protected IQueryBuilder QueryBuilder;

        protected BaseApiController(ICommandBuilder commandBuilder, IQueryBuilder queryBuilder)
        {
            CommandBuilder = commandBuilder;
            QueryBuilder = queryBuilder;
        }

        protected IActionResult ResultFromCommand(CommandResult commandResult, bool returnAdditionIfOk = false)
        {
            if (commandResult.IsSuccessful)
            {
                var result = returnAdditionIfOk ? commandResult.Addition : commandResult;
                return Ok(result);
            }

            return BadRequest(commandResult);
        }
    }
}