using System.Collections.Generic;
using System.Threading.Tasks;
using Cinema.Commands.Seats;
using Cinema.Models.Seats;
using Cinema.Infrastructure.Interfaces;
using Cinema.Queries.Seats;
using Microsoft.AspNetCore.Mvc;

namespace Cinema.Controllers
{
    public class SeatController : BaseApiController
    {
        public SeatController(ICommandBuilder commandBuilder, IQueryBuilder queryBuilder) : base(commandBuilder, queryBuilder)
        {
        }

        /// <summary>
        /// Returns all seats without pagination possibility.
        /// </summary>
        /// <param name="count"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<SeatModel>> GetList(int count)
        {
            var seatsQueryContext = new GetSeatsQueryContext(count);
            var seats = await QueryBuilder.AskAsync<GetSeatsQueryContext, List<SeatModel>>(seatsQueryContext);
            return seats;
        }

        /// <summary>
        /// Reserve or free seat.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> Reserve([FromBody] ReserveSeatModel model)
        {
            var reserveSeatContext = new ReserveSeatCommandContext(model.SeatId);
            var reserveSeatResult = await CommandBuilder.ExecuteAsync(reserveSeatContext);
            return ResultFromCommand(reserveSeatResult);
        }
    }
}