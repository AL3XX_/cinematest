namespace Cinema.Models.Seats
{
    public class SeatModel
    {
        public long Id { get; set; }
        
        public bool IsReserved { get; set; }
    }
}