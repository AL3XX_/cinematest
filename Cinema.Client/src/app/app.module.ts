import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import {HttpClientModule} from "@angular/common/http";
import { SplitSeatsPipe } from './pipes/split-seats.pipe';
import { SplitRowsPipe } from './pipes/split-rows.pipe';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    SplitSeatsPipe,
    SplitRowsPipe,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
