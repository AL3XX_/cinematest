import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from "@angular/common/http";
import {SeatModel} from "../models/seat-model";
import {AppConfiguration} from "../app.configuration";

@Injectable({
  providedIn: 'root'
})
export class SeatService {

  ApiEndpoint: string = AppConfiguration.ServerName + "/api/v1/";

  constructor(private httpClient: HttpClient) { }

  getSeats(count: number) {

    let params = new HttpParams().set('count', count.toString());
    return this.httpClient.get<SeatModel[]>(this.ApiEndpoint + "seat", { params: params });
  }

  reserveSeat(seatId: number) {
    return this.httpClient.post(this.ApiEndpoint + "seat", { seatId: seatId })
  }
}
