import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import './home.component.css';
import {SeatModel} from "../models/seat-model";
import {SeatService} from "../services/seat.service";
import {AppConfiguration} from "../app.configuration";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class HomeComponent implements OnInit {

  Seats: SeatModel[] = null;
  Configuration = AppConfiguration;

  constructor(private seatService: SeatService) {}

  ngOnInit() {
    let seatsCount = this.Configuration.DefaultRowsCount * this.Configuration.DefaultSeatsCount;

    this.seatService.getSeats(seatsCount).subscribe(
      (result) =>
      {
        console.log(result);
        this.Seats = result;
      },
      (error) =>
      {
        console.log(error)
      });
  }

  reserve(rowIndex: number, seatId: number) {

    this.seatService.reserveSeat(seatId).subscribe(
      (result) => {
        console.log(result);

        let seatElement = document.getElementById(`row${rowIndex}_seat${seatId}`);

        if (seatElement.classList.contains("available")) {
          seatElement.classList.remove("available");
          seatElement.classList.add("reserved");

          alert(`You reserved seat with number ${seatId}.`)
        }
        else {
          seatElement.classList.remove("reserved");
          seatElement.classList.add("available");
        }
      },
      (error) => {
        alert("Seems like unknown error occured. Please, try again.");
        console.log(error)
      });
  }
}
