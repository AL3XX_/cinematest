export class AppConfiguration {
  static ServerName: string = "http://localhost:5000";
  static DefaultSeatsCount: number = 20;
  static DefaultRowsCount: number =  40;
}
