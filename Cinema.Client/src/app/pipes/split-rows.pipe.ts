import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'splitRows'
})
export class SplitRowsPipe implements PipeTransform {

  transform(rowsSplitValue: number): any {
    let collection = [];

    for (let i = 0; i < rowsSplitValue; i++)
    {
      collection.push(i);
    }

    return collection;
  }

}
