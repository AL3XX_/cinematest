import { Pipe, PipeTransform } from '@angular/core';
import {SeatModel} from "../models/seat-model";

@Pipe({
  name: 'splitSeats'
})
export class SplitSeatsPipe implements PipeTransform {

  transform(seats: SeatModel[], seatsSplitValue: number): any {
    let collection: SeatModel[] = [];

    if (seats.length > 0) {
      for (let i = 0; i < seatsSplitValue; i++) {
        let seat = seats.shift();
        collection.push(seat);
      }
    }

    return collection;
  }

}
